//
//  SpotifyProApp.swift
//  SpotifyPro
//
//  Created by Hui Chih Wang on 2020/11/25.
//

import SwiftUI

@main
struct SpotifyProApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
