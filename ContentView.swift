//
//  ContentView.swift
//  SpotifyPro
//
//  Created by Hui Chih Wang on 2020/11/25.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
